package com.company;

import com.company.library.UI;

import javax.swing.*;

public class MainWindow {
    JFrame frame = new JFrame("WeatherApp");

    UI userInterface;

    MainWindow() {
        this.userInterface = new UI();
        frame.add(userInterface.getUiElements());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 500);
        frame.setVisible(true);
    }
}