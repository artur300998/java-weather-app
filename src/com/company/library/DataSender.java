package com.company.library;

interface DataSender {
    String sendData() throws Exception;
}
