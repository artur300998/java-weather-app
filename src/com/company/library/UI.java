package com.company.library;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

public class UI {
    private Container uiElements = new Container();
    private HashMap<String, JLabel> dynamicLabels = new HashMap<>();

    synchronized void setUiLabel(String uiLabel, String text) {
        dynamicLabels.get(uiLabel).setText(text);
    }

    public UI() {
        // Init user interface

        uiElements.setLayout(new FlowLayout());

        JButton weatherGettingButton = new JButton("Get current weather");
        uiElements.add(weatherGettingButton);
        weatherGettingButton.addActionListener(new UIUpdater(this));

        uiElements.add(new JLabel("Weather in "));
        JLabel city = new JLabel("");
        uiElements.add(city);
        dynamicLabels.put("city", city);
    }

    public Container getUiElements() {
        return uiElements;
    }


}
