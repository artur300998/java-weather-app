package com.company.library;

import org.json.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class UIUpdater implements Runnable, ActionListener {

    private DataSender dataSender = new WeatherAPIAccess();

    private UI uiToUpdate;

    UIUpdater(UI ui) {
        uiToUpdate = ui;
    }

    void updateUI() {
        Thread t = new Thread(this);
        t.start();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        updateUI();
    }

    @Override
    public void run() {
        String receivedData;
        try {
            receivedData = receiveData();
        } catch (Exception ex) {
            uiToUpdate.setUiLabel("city", "N/A");
            return;
        }

        JSONObject jsonObject = new JSONObject(receivedData);

        uiToUpdate.setUiLabel("city", jsonObject.getString("name"));

    }

    private String receiveData() throws Exception {
        return dataSender.sendData();
    }
}
