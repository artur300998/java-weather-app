package com.company.library;

import java.net.*;
import java.net.http.*;

class WeatherAPIAccess implements DataSender {
    private final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_1_1)
            .build();

    public String sendGet() throws Exception {

        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("http://api.openweathermap.org/data/2.5/weather?q=lodz&appid=1d9cba5a8999ac0e6a5301e31e4676d5"))
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        // print status code
        //System.out.println(response.statusCode());

        // print response body
        //System.out.println(response.body());

        return response.body();
    }

    public String sendData() throws Exception {
        return sendGet();
    }
}
